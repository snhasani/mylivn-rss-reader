#!/usr/bin/env node

import cors from 'cors'
import express from 'express'
import RSSParser from 'rss-parser'
import {getErrorMeta} from './get-error-meta.mjs'

const app = express();
const rssParser = new RSSParser();

app.use(
  cors({
    origin: true,
    credentials: true,
    maxAge: 1000 * 60 * 5,
  }),
);

app.get('/feed', (req, res) => {
  rssParser.parseURL(req.query.url, (error, feed) => {
    if (error) {
      const errorMeta = getErrorMeta(error)
      res.status(errorMeta.status).json(errorMeta)
    }

    res.json(feed)
  });
});

const server = app.listen(process.env.PORT || 9000);

const close = () => {
  server.close(() => {
    process.exit(0);
  });
};

process.on('SIGINT', close);
process.on('SIGTERM', close);
