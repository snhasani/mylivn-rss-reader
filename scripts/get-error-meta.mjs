export function getErrorMeta(error) {
  if (error.message === 'Status code 404') {
    return {
      status: 404,
      message: "URL not found!"
    }
  }

  if (error.message.includes('Unquoted')) {
    return {
      status: 400,
      message: "Invalid request!"
    }
  }

  return {
    status: 500,
    message: "Something went wrong. Please try again."
  }
}
