# RSS Reader Web APP

A React RSS Reader Web APP for Mylivn company frontend test assignment.

## Up and running

Install the project dependencies:

**NOTE**: Please try running the project with the latest versions of
Node.js. I developed it on version 12, and I can't fully guarantee that it will
work in the older versions. But you’ll need to have at least Node >= 8.10 on your local development machine, anyway.

You can use [nvm](https://github.com/creationix/nvm#installation) (macOS/Linux)
or
[nvm-windows](https://github.com/coreybutler/nvm-windows#node-version-manager-nvm-for-windows)
to switch Node versions between different projects.

After installing a proper version of Node.js on your machine, you can run:

```
npm install
```

And at the end for up and running the production build, run this command:

```
npm run demo
```

Then open your favorite web browser and go to
[localhost:8080](http://localhost:8080/)

## Local Development

Below is a list of commands you will probably find useful for up and running
development server and tests.

### `npm start`

Builds and Runs React App alongside a proxy server for parsing RSS feeds in the development mode.

It watches changes by default and reloads the servers and browser window as soon
as possible, the app compiled.

By default, the app is running on `localhost:3000` address.

### `npm run build`

Compiles and bundles the production version into the `public` folder.

### `npm test`

Runs all tests by Jest.

### `npm test:watch`

Runs the test watcher (Jest) in an interactive mode. By default, runs tests
related to files changed since the last commit.

### `npm test:coverage`

Runs all tests and showing total test coverage.

You can find HTML report of
the test coverage in `coverage/lcov-report` directory. You just need to open the
`index.html` file.

### `npm run demo`

Builds and runs a production build of the app on `localhost:8080` address.

### `npm run develop`

Only runs the client development server on `localhost:3000` address and watches changes.

### `npm run proxy`

Only runs the proxy server on `localhost:9000` address and watches changes.

### `npm run format`

Formats the codes with the Prettier's magic.

By default, you don't need to run
this command. Formatting code is handling by the `lint-staged` and husky on the
git pre-commit hook.
