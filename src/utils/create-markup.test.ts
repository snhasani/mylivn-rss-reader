import { createMarkup } from "./create-markup";

test("should return a empty string value for undefined input", () => {
  expect(createMarkup()).toMatchObject({ __html: "" });
});
