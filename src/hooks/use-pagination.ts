import { useState, useMemo, useEffect } from "react";

import { PAGINATION_PAGE_SIZE } from "../constants";
export interface CurrentPageMeta {
  from: number;
  isFirstPage: boolean;
  isLastPage: boolean;
  pageNumber: number;
  to: number;
}

export interface CurrentPage<T> {
  items: T[];
  meta: CurrentPageMeta;
}

export function usePagination<T>(
  items: T[],
  pageSize: number = PAGINATION_PAGE_SIZE
) {
  const totalNumberOfItems = items.length;
  const [currentPageNumber, setCurrentPageNumber] = useState(1);

  const currentPage: CurrentPage<T> = useMemo(() => {
    let isLastPage = true;
    let isFirstPage = true;
    let currentPageItems = items;
    const startIndex = (currentPageNumber - 1) * pageSize;
    const endIndex = Math.min(currentPageNumber * pageSize, totalNumberOfItems);

    if (pageSize > 0 && totalNumberOfItems > pageSize) {
      currentPageItems = items.slice(startIndex, endIndex);

      isFirstPage = currentPageNumber === 1;
      isLastPage =
        Math.ceil(totalNumberOfItems / pageSize) === currentPageNumber;
    }

    return {
      items: currentPageItems,
      meta: {
        pageNumber: currentPageNumber,
        from: startIndex + 1,
        isLastPage,
        isFirstPage,
        to: endIndex,
      },
    };
  }, [currentPageNumber, items, pageSize, totalNumberOfItems]);

  useEffect(() => {
    setCurrentPageNumber(1);
  }, [items, pageSize]);

  const onNextPage = () => {
    if (!currentPage.meta.isFirstPage) {
      setCurrentPageNumber(currentPageNumber - 1);
    }
  };

  const onPreviousPage = () => {
    if (!currentPage.meta.isLastPage) {
      setCurrentPageNumber(currentPageNumber + 1);
    }
  };

  return {
    currentPage,
    onNextPage,
    onPreviousPage,
    totalNumberOfItems,
  };
}
