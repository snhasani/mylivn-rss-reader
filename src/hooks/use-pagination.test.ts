import { renderHook, act } from "@testing-library/react-hooks";

import { PAGINATION_PAGE_SIZE } from "../constants";
import { usePagination } from "./use-pagination";

const IETMS_NUMBER = 17;
const ITEMS = Array.from(Array(IETMS_NUMBER).keys()).map((i) => ++i);

describe("usePagination", () => {
  test("should initialize the hook with default page size successfully", () => {
    const { result } = renderHook(() => usePagination<number>(ITEMS));

    const {
      currentPage,
      onNextPage,
      onPreviousPage,
      totalNumberOfItems,
    } = result.current;

    // Default page size is 5
    expect(currentPage.items).toHaveLength(PAGINATION_PAGE_SIZE);
    expect(currentPage.meta.from).toBe(1);
    expect(currentPage.meta.to).toBe(PAGINATION_PAGE_SIZE);
    expect(currentPage.meta.pageNumber).toBe(1);
    expect(currentPage.meta.isFirstPage).toBe(true);
    expect(currentPage.meta.isLastPage).toBe(false);
    expect(typeof onNextPage).toBe("function");
    expect(typeof onPreviousPage).toBe("function");
    expect(totalNumberOfItems).toBe(IETMS_NUMBER);
  });

  test("should return both isFirstPage and isLastPage true when page size is bigger than the total number of items", () => {
    const { result } = renderHook(() => usePagination(ITEMS, 20));

    const { currentPage, totalNumberOfItems } = result.current;

    expect(currentPage.items).toHaveLength(ITEMS.length);
    expect(currentPage.meta.from).toBe(1);
    expect(currentPage.meta.to).toBe(ITEMS.length);
    expect(currentPage.meta.pageNumber).toBe(1);
    expect(currentPage.meta.isFirstPage).toBe(true);
    expect(currentPage.meta.isLastPage).toBe(true);
    expect(totalNumberOfItems).toBe(IETMS_NUMBER);
  });

  test("should return next page items after onNextPage event is triggered", () => {
    const { result } = renderHook(() =>
      usePagination(ITEMS, PAGINATION_PAGE_SIZE)
    );

    act(() => {
      const { onPreviousPage } = result.current;
      onPreviousPage();
    });

    const { currentPage } = result.current;

    const from = PAGINATION_PAGE_SIZE + 1;
    const to = from + PAGINATION_PAGE_SIZE - 1;
    expect(currentPage.meta.from).toBe(from);
    expect(currentPage.meta.to).toBe(to);
    expect(currentPage.meta.pageNumber).toBe(2);
    expect(currentPage.meta.isFirstPage).toBe(false);
    expect(currentPage.meta.isLastPage).toBe(false);
  });

  test("should return prevoues page items after onPreviousPage event is triggered", () => {
    const { result } = renderHook(() =>
      usePagination(ITEMS, PAGINATION_PAGE_SIZE)
    );

    expect(result.current.currentPage.meta.isFirstPage).toBe(true);

    act(() => {
      result.current.onPreviousPage();
    });

    expect(result.current.currentPage.meta.pageNumber).toBe(2);
    expect(result.current.currentPage.meta.isFirstPage).toBe(false);

    act(() => {
      result.current.onNextPage();
    });

    const { currentPage } = result.current;

    expect(currentPage.meta.from).toBe(1);
    expect(currentPage.meta.pageNumber).toBe(1);
    expect(currentPage.meta.isFirstPage).toBe(true);
    expect(currentPage.meta.to).toBe(PAGINATION_PAGE_SIZE);
  });

  test("should not fire the onNextPage event if it is already in the first page", () => {
    const { result } = renderHook(() =>
      usePagination(ITEMS, PAGINATION_PAGE_SIZE)
    );

    expect(result.current.currentPage.meta.pageNumber).toBe(1);
    expect(result.current.currentPage.meta.isFirstPage).toBe(true);

    act(() => {
      result.current.onNextPage();
    });

    expect(result.current.currentPage.meta.pageNumber).toBe(1);
    expect(result.current.currentPage.meta.isFirstPage).toBe(true);
  });

  test("should not fire the onPreviousPage event if it is already in the last page", () => {
    const ITEMS = [1, 2];
    const PAGE_SIZE = 1;
    const { result } = renderHook(() => usePagination(ITEMS, PAGE_SIZE));

    expect(result.current.currentPage.meta.pageNumber).toBe(1);
    expect(result.current.currentPage.meta.isLastPage).toBe(false);
    expect(result.current.currentPage.meta.isFirstPage).toBe(true);

    act(() => {
      result.current.onPreviousPage();
    });

    expect(result.current.currentPage.meta.pageNumber).toBe(2);
    expect(result.current.currentPage.meta.isLastPage).toBe(true);
    expect(result.current.currentPage.meta.isFirstPage).toBe(false);

    act(() => {
      result.current.onPreviousPage();
    });

    expect(result.current.currentPage.meta.pageNumber).toBe(2);
    expect(result.current.currentPage.meta.isLastPage).toBe(true);
    expect(result.current.currentPage.meta.isFirstPage).toBe(false);
  });
});
