import { useState, useEffect } from "react";
import Parser from "rss-parser";

import { getFeed } from "api";

export function useGetRssFeed(initialUrl: string = "") {
  const [url, setUrl] = useState(initialUrl);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<Error | null>(null);
  const [feed, setFeed] = useState<Parser.Output | null>(null);

  useEffect(() => {
    let didCancel = false;

    const getData = async () => {
      try {
        setIsLoading(true);
        setError(null);

        const feed = await getFeed(url);

        if (!didCancel) {
          setFeed(feed);
          setIsLoading(false);
        }
      } catch (error) {
        if (!didCancel) {
          setError(error);
          setIsLoading(false);
        }
      }
    };

    if (url) {
      getData();
    }

    return () => {
      didCancel = true;
    };
  }, [url]);

  return { feed, error, isLoading, setUrl };
}
