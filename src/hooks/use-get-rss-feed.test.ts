import { renderHook, act } from "@testing-library/react-hooks";

import { getFeed } from "../api/get-feed";
import { useGetRssFeed } from "./use-get-rss-feed";

jest.mock("../api/get-feed");

const mockedGetFeed = getFeed as jest.Mock;

afterEach(() => jest.clearAllMocks());

describe("useGetRssFeed", () => {
  test("should initialize the hook successfully", async () => {
    const { result } = renderHook(() => useGetRssFeed());

    const { feed, error, isLoading, setUrl } = result.current;

    expect(feed).toBeNull();
    expect(error).toBeNull();
    expect(isLoading).toBeFalsy();
    expect(typeof setUrl).toBe("function");
  });

  test("should return feed items after setting an URL", async () => {
    mockedGetFeed.mockResolvedValueOnce({ items: [] });

    const { result, waitForNextUpdate } = renderHook(() => useGetRssFeed());

    act(() => {
      const { setUrl } = result.current;
      setUrl("something");
    });

    expect(result.current.isLoading).toBeTruthy();

    await waitForNextUpdate();

    expect(result.current.isLoading).toBeFalsy();
    expect(result.current.feed).toEqual({ items: [] });
    expect(mockedGetFeed).toHaveBeenCalledTimes(1);
    expect(mockedGetFeed).toHaveBeenCalledWith("something");
  });

  test("should unmount the hook if a new URL set during fetching the previous one", async () => {
    mockedGetFeed.mockResolvedValueOnce({ items: [] });

    const { result, unmount } = renderHook(() => useGetRssFeed());

    act(() => {
      const { setUrl } = result.current;
      setUrl("something");
    });

    act(() => {
      const { setUrl } = result.current;
      setUrl("something else");
    });

    unmount();
  });

  test("should got an error for a not found URL", async () => {
    const ERROR = { status: 404, message: "URL not found!" };
    mockedGetFeed.mockRejectedValueOnce(ERROR);

    const { result, waitForNextUpdate } = renderHook(() => useGetRssFeed());

    act(() => {
      const { setUrl } = result.current;
      setUrl("something");
    });

    expect(result.current.isLoading).toBeTruthy();

    await waitForNextUpdate();

    expect(result.current.feed).toBeNull();
    expect(result.current.isLoading).toBeFalsy();
    expect(result.current.error).toMatchObject(ERROR);
    expect(mockedGetFeed).toHaveBeenCalledTimes(1);
    expect(mockedGetFeed).toHaveBeenCalledWith("something");
  });

  test("should unmount the hook if the fech request rejected", async () => {
    mockedGetFeed.mockRejectedValueOnce({});

    const { result, unmount } = renderHook(() => useGetRssFeed());

    act(() => {
      const { setUrl } = result.current;
      setUrl("something");
    });

    act(() => {
      const { setUrl } = result.current;
      setUrl("something else");
    });

    unmount();
  });
});
