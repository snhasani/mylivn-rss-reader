import React, { useState, FormEvent, ChangeEvent } from "react";

import {
  Container,
  Label,
  Input,
  ArrowIcon,
  FieldBox,
  SubmitButton,
} from "./styles";

interface Props {
  handleSubmit: (url: string) => void;
}

function UrlField({ handleSubmit }: Props) {
  const [url, setUrl] = useState("");

  const onSubmit = (event: FormEvent) => {
    event.preventDefault();
    handleSubmit(url);
  };

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    setUrl(event.target.value.trim());
  };

  return (
    <Container onSubmit={onSubmit}>
      <Label htmlFor="rss-url">Type in your RSS feed URL:</Label>
      <FieldBox>
        <Input
          value={url}
          id="rss-url"
          autoComplete="off"
          onChange={onChange}
          placeholder="Enter a RSS feed URL"
          data-testid="url-field-input"
        />
        <SubmitButton type="submit" data-testid="url-field-submit-btn">
          <ArrowIcon />
        </SubmitButton>
      </FieldBox>
    </Container>
  );
}

export default UrlField;
