import styled from "styled-components";

const AppMain = styled.main`
  max-width: 1024px;
  margin: 0 auto;
  padding: 1.5rem;
`;

export default AppMain;
