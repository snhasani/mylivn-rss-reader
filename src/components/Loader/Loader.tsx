import React from "react";
import styled from "styled-components";

import { ReactComponent as Spinner } from "../../assets/images/icons/circle-spinner.svg";

const Container = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  padding-top: 15%;
`;

function Loader() {
  return (
    <Container>
      <Spinner width="96px" height="96px" />
    </Container>
  );
}

export default Loader;
