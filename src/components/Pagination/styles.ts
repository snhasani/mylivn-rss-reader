import styled from "styled-components";

export const Container = styled.div`
  color: #a0a0a0;
  display: flex;
  align-items: center;
`;

export const Button = styled.button`
  border: none;
  color: #5f5f5f;
  padding: 0.5em 0.75em;
  margin-left: 0.5em;
  border-radius: 3px;
  background-color: transparent;
  transition: background-color 220ms ease-in;

  &:disabled {
    color: #ccc;
  }

  &:not([disabled]):hover {
    cursor: pointer;
    background-color: #f1f1f1;
  }
`;
