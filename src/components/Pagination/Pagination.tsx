import React from "react";
import { CurrentPageMeta } from "hooks/use-pagination";

import { Button, Container } from "./styles";

interface Props extends Omit<CurrentPageMeta, "pageNumber"> {
  onNextPage: () => void;
  onPreviousPage: () => void;
  totalNumberOfItems: number;
}

function Pagination({
  from,
  to,
  isFirstPage,
  isLastPage,
  onNextPage,
  onPreviousPage,
  totalNumberOfItems,
}: Props) {
  return (
    <Container>
      <span>{`${from} - ${to} of ${totalNumberOfItems}`}</span>
      <Button onClick={onNextPage} disabled={isFirstPage}>
        Newer
      </Button>
      <Button onClick={onPreviousPage} disabled={isLastPage}>
        Older
      </Button>
    </Container>
  );
}

export default Pagination;
