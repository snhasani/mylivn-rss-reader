import React from "react";
import { render } from "@testing-library/react";

import ErrorBoundary from "./ErrorBoundary";

beforeAll(() => {
  jest.spyOn(console, "error").mockImplementation(() => {});
});

afterAll(() => {
  console.error.mockRestore();
});

afterEach(() => {
  jest.clearAllMocks();
});

function Bomb({ shouldThrow }: { shouldThrow?: boolean }) {
  if (shouldThrow) {
    throw new Error("💣");
  } else {
    return null;
  }
}

test("renders that there was a problem", () => {
  const { rerender, getByRole } = render(
    <ErrorBoundary>
      <Bomb />
    </ErrorBoundary>
  );

  rerender(
    <ErrorBoundary>
      <Bomb shouldThrow />
    </ErrorBoundary>
  );

  expect(console.error).toHaveBeenCalledTimes(3);

  expect(getByRole("alert").textContent).toMatchInlineSnapshot(
    `"Something went wrong. Please try again."`
  );

  console.error.mockClear();

  rerender(
    <ErrorBoundary>
      <Bomb />
    </ErrorBoundary>
  );
});
