import React, { ErrorInfo } from "react";
import { ErrorMessage } from "components/ErrorMessage";

interface Props {}
interface State {
  hasError: boolean;
}

class ErrorBoundary extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error: Error) {
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.error(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      return (
        <ErrorMessage>Something went wrong. Please try again.</ErrorMessage>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
