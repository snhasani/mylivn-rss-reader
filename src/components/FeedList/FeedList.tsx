import React from "react";
import Parser from "rss-parser";

import { PAGINATION_PAGE_SIZE } from "../../constants";

import { usePagination } from "hooks/use-pagination";
import { Loader } from "components/Loader";
import { FeedItem } from "components/FeedItem";
import { Pagination } from "components/Pagination";
import { ErrorMessage } from "components/ErrorMessage";
import { ErrorBoundary } from "components/ErrorBoundary";

import { Header, Title } from "./styles";

interface Props {
  feed: Parser.Output | null;
  isLoading: boolean;
  error: Error | null;
}

function FeedList({ feed, isLoading, error }: Props) {
  const {
    currentPage,
    onNextPage,
    onPreviousPage,
    totalNumberOfItems,
  } = usePagination<Parser.Item>(feed?.items ?? [], PAGINATION_PAGE_SIZE);

  if (isLoading) {
    return <Loader />;
  }

  if (error) {
    return <ErrorMessage>{error.message}</ErrorMessage>;
  }

  if (!feed || !feed.items) {
    return null;
  }

  return (
    <ErrorBoundary>
      <article>
        <Header>
          <Title>{feed.title}</Title>
          {totalNumberOfItems > PAGINATION_PAGE_SIZE && (
            <Pagination
              from={currentPage.meta.from}
              isFirstPage={currentPage.meta.isFirstPage}
              isLastPage={currentPage.meta.isLastPage}
              onNextPage={onNextPage}
              onPreviousPage={onPreviousPage}
              to={currentPage.meta.to}
              totalNumberOfItems={totalNumberOfItems}
            />
          )}
        </Header>
        <section>
          {currentPage.items.map((item: Parser.Item, index: number) => (
            <FeedItem key={index} item={item} />
          ))}
        </section>
      </article>
    </ErrorBoundary>
  );
}

export default FeedList;
