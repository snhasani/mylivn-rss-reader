import React from "react";
import styled from "styled-components";

const Box = styled.div`
  text-align: center;
  color: red;
`;

interface Props {
  children: React.ReactNode;
}

function ErrorMessage({ children }: Props) {
  return <Box role="alert">{children}</Box>;
}

export default ErrorMessage;
