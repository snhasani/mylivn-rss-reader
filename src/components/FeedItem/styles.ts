import styled from "styled-components";

export const Container = styled.article`
  background: #fff;
  border-radius: 3px;
  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
  padding: 1rem;
  margin-bottom: 1.5rem;
  line-height: 1.5;
`;

export const Header = styled.header`
  margin-bottom: 1rem;
`;

export const TitleContainer = styled.div`
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
`;

export const TitleText = styled.h3`
  margin-bottom: 0.25rem;
`;

export const ItemLink = styled.a`
  display: flex;
  align-items: center;
  margin-left: 0.5rem;

  & > svg {
    cursor: pointer;
    fill: #a0a0a0;

    &:hover {
      fill: blue;
    }
  }

  &:hover {
    text-decoration: none;
  }
`;

export const Time = styled.time`
  color: #a0a0a0;
  font-size: 0.875rem;
`;

export const Content = styled.section`
  margin: 0;
`;
