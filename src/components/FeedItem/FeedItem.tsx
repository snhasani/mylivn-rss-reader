import React from "react";
import Parser from "rss-parser";

import { ReactComponent as OpenInNewIcon } from "../../assets/images/icons/open-in-new.svg";
import { createMarkup } from "../../utils/create-markup";
import {
  Container,
  Header,
  Time,
  TitleContainer,
  TitleText,
  ItemLink,
  Content,
} from "./styles";

interface Props {
  item: Parser.Item;
}

function FeedItem({ item }: Props) {
  const date = item.isoDate && new Date(item.isoDate).toLocaleString();

  return (
    <Container data-testid="feed-item">
      <Header>
        <TitleContainer>
          <TitleText>{item.title}</TitleText>
          <ItemLink href={item.link} target="_blank" rel="noopener noreferrer">
            <OpenInNewIcon />
          </ItemLink>
        </TitleContainer>
        <Time dateTime={date}>{date}</Time>
      </Header>
      <Content dangerouslySetInnerHTML={createMarkup(item.content)} />
    </Container>
  );
}

export default FeedItem;
