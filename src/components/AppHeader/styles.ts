import styled from "styled-components";

export const Container = styled.header`
  padding: 0 1.5rem;
  height: 108px;
  display: flex;
  align-items: center;
`;

export const Logo = styled.img`
  width: 72px;
  height: 72px;
  margin-left: -0.5rem;
  margin-right: 0.5rem;
`;

export const Title = styled.h1`
  margin: 0;
  font-size: 2rem;
`;
