import React from "react";

import logo from "../../assets/images/logo.png";
import { Container, Logo, Title } from "./styles";

function AppHeader() {
  return (
    <Container>
      <Logo src={logo} alt="logo" />
      <Title>Mylivn RSS Reader</Title>
    </Container>
  );
}

export default AppHeader;
