import React from "react";

import { useGetRssFeed } from "hooks/use-get-rss-feed";
import { AppHeader } from "components/AppHeader";
import { AppMain } from "components/AppMain";
import { UrlField } from "components/UrlField";
import { FeedList } from "components/FeedList";

function App() {
  const { feed, error, isLoading, setUrl } = useGetRssFeed();

  return (
    <>
      <AppHeader />
      <AppMain>
        <UrlField handleSubmit={setUrl} />
        <FeedList feed={feed} isLoading={isLoading} error={error} />
      </AppMain>
    </>
  );
}

export default App;
