import React from "react";
import {
  teardown as teardownDevServer,
  setup as setupDevServer,
} from "jest-dev-server";
import { cleanup, render, screen } from "@testing-library/react";
import user from "@testing-library/user-event";

import App from "../App";
import { API_SERVER_PORT, API_SERVER_HOST } from "../constants";

describe("App", () => {
  beforeAll(async () => {
    await setupDevServer({
      command: `PORT=${API_SERVER_PORT} npm run proxy`,
      debug: true,
      port: API_SERVER_PORT,
      host: API_SERVER_HOST,
      usedPortAction: "kill",
    });
  });

  afterAll(async () => await teardownDevServer());

  afterAll(() => {
    cleanup();
  });

  test("Should render <App /> without crash", () => {
    render(<App />);

    expect(screen.getByText("Mylivn RSS Reader")).toBeInTheDocument();
  });

  test("should render RSS feed items", async () => {
    render(<App />);

    const $input = screen.getByTestId("url-field-input");
    const $submitBtn = screen.getByTestId("url-field-submit-btn");

    user.type($input, "https://hnrss.org/newest");
    user.click($submitBtn);

    const allItems = await screen.findAllByTestId("feed-item", undefined, {
      timeout: 5000,
    });
    expect(allItems.length).toBeGreaterThan(1);
  });

  test("should render a not found error", async () => {
    render(<App />);

    const $input = screen.getByTestId("url-field-input");
    const $submitBtn = screen.getByTestId("url-field-submit-btn");

    user.type($input, "https://hnrss.org/nothing");
    user.click($submitBtn);

    const $alert = await screen.findByRole("alert");
    expect($alert).toHaveTextContent("URL not found!");
  });
});
