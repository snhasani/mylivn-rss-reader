import Parser from "rss-parser";

import { API_SERVER_URL } from "../constants";

export async function getFeed(rssUrl: string): Promise<Parser.Output> {
  const url = `${API_SERVER_URL}/feed?url=${rssUrl}`;

  try {
    const response = await fetch(url);
    const responseBody = await response.json();

    if (response.status === 200) {
      return responseBody;
    } else {
      throw new Error(responseBody.message);
    }
  } catch (error) {
    throw error;
  }
}
