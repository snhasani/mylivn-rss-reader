import { API_SERVER_URL } from "../constants";
import { getFeed } from "./get-feed";

const RSS_URL = "https://hnrss.org/newest";

describe("get city api", () => {
  beforeAll(() => jest.spyOn(window, "fetch"));

  test("fetch hacker news feed", async () => {
    (window.fetch as jest.Mock).mockResolvedValueOnce({
      ok: true,
      status: 200,
      json: async () => ({ items: [] }),
    });

    const url = `${API_SERVER_URL}/feed?url=${RSS_URL}`;

    expect.assertions(4);
    await expect(getFeed(RSS_URL)).resolves.toMatchObject({ items: [] });
    expect(window.fetch).toHaveBeenCalledWith(url);
    expect(window.fetch).toHaveBeenCalledTimes(1);
    expect(window.fetch).toHaveReturned();
  });

  test("returns rss not found error", async () => {
    (window.fetch as jest.Mock).mockResolvedValueOnce({
      ok: false,
      status: 404,
      statusText: "Not Found",
      json: async () => ({
        status: "404",
        message: "URL not found!",
      }),
    });

    expect.assertions(1);
    await expect(getFeed(`${RSS_URL}1`)).rejects.toEqual(
      new Error("URL not found!")
    );
  });
});
