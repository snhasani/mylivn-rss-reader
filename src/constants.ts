export const API_SERVER_HOST = process.env.REACT_APP_API_HOST || "localhost";
export const API_SERVER_PROTOCOL = process.env.REACT_APP_API_PROTOCOL || "http";

export const API_SERVER_PORT = parseInt(
  process.env.REACT_APP_API_PORT || "9000"
);

export const API_SERVER_URL = `${API_SERVER_PROTOCOL}://${API_SERVER_HOST}:${API_SERVER_PORT}`;

export const PAGINATION_PAGE_SIZE = parseInt(
  process.env.REACT_APP_PAGINATION_PAGE_SIZE || "5"
);
